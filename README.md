# Directory map

`reproduce.sh` - this file will run all the reproduction scripts in an appropriate order;


`filter.py` - reproduction script that filters raw counts;


`normalize.r` - reproduction script in R that normalizes filetered raw counts;


`main.py` - reproduction script that builds figures and datatables for the neigbhourhood logfoldchange statistic;


`de_join.r` - reproduction script in R that runs exact tests on neighbours;


`main_de.py` - reproduction script for differential expression data;


`requirements.txt` - required packages by Python reproduction scrits;


`TMM_CPM.txt` - TMM & CPM-normalized gene expression across different time frames;


`TMM_CPM_samples.txt` - TMM norm factors;


`de_join.txt` - FDR values for exact tests by **edgeR**;


`de_lfc_join.txt` - logfoldchanges for neighbours as produced by the **edgeR**;


`output` - figures & and datatables;


`output/tails` - KDE plots for logfoldchange neighbor statistic for each time frame with number of genes in both tails;


`output/foldchanges_rel.csv`- datatable with logfoldchange statistics as computed by the reproduction script;


`output/tail_counts.csv` - datable with a number of genes in left and right tails of the logfoldchange statistic distribution;


`output/violin.png` - violin plots for N-batches (colored in red) together with the closest non-N-batch (coloured in blue);


`output/barplot.png` - barplot with average logfoldchanges across neighbours for each time frame. N-batches are coloured in red;


`output/marginals_scatter.png` - huge 5-columns plot. First two plots are KDEs of an N-batch and it's 40-minute behind & ahead neighbours, for both centered and non-centered statistics (though they appear to be very similar). Note that the 40-minute gap was chosen to ensure that logfoldchange statistics don't overlap with an N-batch. The last 3 plots are scatterplots build between those 3 logfoldchange statistics;


`output/fraction.png` - plotted fraction of genes in tails conditioned on a "definition" of tail: at x-axis, is a cutoff-value - we say that eveyrthing left and right from the negative cutoff values and positive cutoff value is a tail;


`output/de_barplot.png` - bar plots for a number of differentially expressed genes across time frames versus neighbour time frames. The plot is separated in two parts, the upper part is for positive logfoldchanges, and the lower is for negative logfoldchanges;


`output/de_time_frames.png` - histogram for a number of genes that are differentially expressed at x time frames;


`output/genes_go.csv` - tabular file with genes as rows (only top 500 most frequently differentially expressed genes are included) and columns as a number of DE time frames, number of GOs and a list of GOs the gene is affiliated with;


`output/gos.csv` - tabular file with GOs as rows and columns as a number of genes that were found amongst top 500 most frequently differentially expressed genes.


# Reproduction instructions

Obtained plots and tables are exactly reproducible via evaluating the `filter.py`, `normalize.r`, `main.py`, `de_join.r`, `main_de.r`  scripts in the order as specified.

Or, alternatively, it can be done as easily as running the `reproduce.sh`:


>./reproduce.sh

Make sure that you have **edgeR** package for R and **tqdm**, **seaborn**, **datatable** and  **intermine** for Python3. Note that as of the latter, **intermine** seems to be bugged with the latest version of Python3, and if it happens to raise a `StopIteration` error, just wrap around the faulty `group` method in `query.py` with `try... except StopIteration: pass` block. 


# Details

- Genes were filtered if they did not met the following criteria: at least one biological replicate within a time frame vicinity (i.e. within 20 minutes) should have cpm greater than 1.0 (configurable via `cpm_cutoff` parameter at `filter.py`);

- The statistic we used for the analysis is computed for each timeframe/"batch" as an average logfoldchange to its neighbours (i.e. points that stay off for 20 minutes);

- For building `tail_counts.csv` and plots in `output/tails`, we assumed logfoldchange values to lie in tails if they fall either below `tail_low` (left tail) or above `tail_high` (right tail) cutoff-values that are changeable at the beginning of the reproduction script;

- When plotting `fraction.png`, we counted union of genes in tails across time frames. There are in total 23 time frames that keep away from N-frames for at least 40 minutes, and 12 N-frames in total. As a number of non-N-batch time frames is almost 2 times greater, there is a higher chance to see more genes in it by random. Hence, we included only odd time frames in non-N-batches, resulting in equal number of time frames in both N-batches and non-N-batches. 

- We used webapi of **thalemine** to extract GOs for the top 500 most frequently DE at N-batches genes. The number can be tuned in `main_de.py`;

# Results

Here we show some figures from the output folder. Notice that their resolution is not maxed here and you can open them directly for inspection.

## Violin plots

![Violin plot](./output/violin.png)

## Bar plot

![Bar plot](./output/barplot.png)

## Fraction plot

![Fraction plot](./output/fraction.png)


## KDE marginals & scatter plots
![KDE& scatter plots](./output/marginals_scatter.png)

## Tail counts
We don't show figures for each of the 70 statistics - you can find them in `output/tails` folder.

## Number of differentially expressed genes at different timeframes

![DE counts at different time frames](./output/de_barplot.png)

The barplot is separated in 2 plots - one for those genes that exhibited positive logfoldchanges, and the other (lower) -- for negative logfoldchanges.


## Number of genes that are differentially expressed at multiple time frames

![DE counts at multiple time frames](./output/de_time_frames.png)

Notice that most genes at non-N time frames are not differentially expressed.


## Is DE at N time frames specific to a particular GO?

We find it unlikely and one can see the evidence for it in tables `gos.csv` and `genes_go.csv`.
Here, we provide number of genes discovered at top 60 most frequent GOs (out of total ~1000 to be found in `gos.csv`):

```
                                                    Gene count
nucleus                                                    165
integral component of membrane                             137
molecular_function                                         118
biological_process                                         107
plasma membrane                                            101
protein binding                                             90
chloroplast                                                 88
cytosol                                                     88
extracellular region                                        79
cytoplasm                                                   77
mitochondrion                                               74
plasmodesma                                                 40
metal ion binding                                           37
membrane                                                    36
endoplasmic reticulum                                       36
Golgi apparatus                                             36
DNA-binding transcription factor activity                   30
mRNA binding                                                29
regulation of transcription, DNA-templated                  29
cellular_component                                          27
vacuole                                                     26
cell wall                                                   23
ATP binding                                                 23
response to abscisic acid                                   18
vacuolar membrane                                           18
plant-type cell wall                                        17
plastid                                                     17
chloroplast envelope                                        17
transmembrane transport                                     17
DNA binding                                                 16
chloroplast stroma                                          15
endoplasmic reticulum membrane                              15
apoplast                                                    15
defense response                                            14
trans-Golgi network                                         14
cell wall organization                                      14
sequence-specific DNA binding                               13
oxidation-reduction process                                 12
protein ubiquitination                                      11
copper ion binding                                          10
multicellular organism development                          10
structural constituent of ribosome                          10
ATPase activity                                             10
chloroplast thylakoid membrane                              10
zinc ion binding                                            10
nucleolus                                                   10
endosome                                                    10
response to light stimulus                                  10
secretory vesicle                                            9
lipid binding                                                9
unidimensional cell growth                                   9
transferase activity, transferring glycosyl groups           9
embryo development ending in seed dormancy                   9
response to water deprivation                                9
transcription regulatory region sequence-specif...           9
response to cold                                             9
oxidoreductase activity                                      9
RNA binding                                                  9
kinase activity                                              8
vesicle-mediated transport                                   8
```
 As one can see, as soon as very general GOs like "biological_function" are gone, most GOs have a relatively uniform distribution.
